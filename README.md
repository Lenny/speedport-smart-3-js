# speedport-smart-3-js

Interface with Telekom Speedport Smart 3 routers

Example: 

```js
import { Smart3 } from "speedport-smart-3"

const router = new Smart3(
    "http://192.168.2.1",
    "secretpassword1337"
)

let main = async function() {
    for (const device of await router.getDeviceList()) {
        console.log(device);
    }
};

main();
```

## Credits
Large portions of this codebase were adapted from this repo: https://github.com/hacki11/ioBroker.speedport
`Copyright (c) 2021 hacki11 jur.schmid@gmail.com`